﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MultiStepForm._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <script src="Scripts/MultiStep.js"></script>

    <link href="Content/MultiStep.css" rel="stylesheet" />
   <div class="tabbable">
  <ul class="nav nav-tabs wizard">
    <li class="active"><a href="#i9" data-toggle="tab" aria-expanded="false"><span class="nmbr">1</span>Step 01</a></li>
    <li><a href="#w4" data-toggle="tab" aria-expanded="false"><span class="nmbr">2</span>Step 02</a></li>
    <li><a href="#stateinfo" data-toggle="tab" aria-expanded="false"><span class="nmbr">3</span>Step 03</a></li>
    <li><a href="#companydoc" data-toggle="tab" aria-expanded="false"><span class="nmbr">4</span>Step 04</a></li>
    <li><a href="#finish" data-toggle="tab" aria-expanded="true"><span class="nmbr">5</span>Step 05</a></li>

  </ul>
   
</asp:Content>

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MultiStepForm.Startup))]
namespace MultiStepForm
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
